# SAE 302

Ce site a pour objectifs de mieux comprendre l'utilisation de systèmes de vues mais aussi de mieux utiliser PHP.

L'objectif final de ce site était la création d'un système de connexion, d'inscription et de déconnexion.

Le site devait contenir les fonctionnalités d'ajouter des photos sur le site et de pouvoir effectuer un vote avec des étoiles.
