<?php
//on va placer dans ce fichier trois controleurs: inscription, connexion, deconnexion
// on charge le modèle contenant les différentes fonctions
//liées à l'authentification
require('application/modeles/utilisateurs.php');

function inscription() {
    if (empty($_POST)) {
        // si on a reçu aucune donnée via un formulaire, on affiche le formulaire d'inscription 
        require('application/vues/pageInscription.php');
    } else {
        // on vérifie que tous les champs sont remplis
        if (empty($_POST['pseudo']) || empty($_POST['mail']) || empty($_POST['pw1']) || empty($_POST['pw2'])) {
            // on vide le tableau $_POST pour éviter une boucle de redirection infinie
            $_POST = [];
            $_SESSION['error'] = "Merci de renseigner tous les champs";
            header('Location:'.baseURL.'index.php?route=inscription');
        }

        // on vérifie la correspondance des deux mots de passe
            if ($_POST['pw1'] != $_POST['pw2']) {
                $_SESSION['error'] = "Les mots de passe ne correspondent pas";
                $_POST = [];
                header('Location:'.baseURL.'index.php?route=inscription');
            }

            // on vérifie que ni le mail ni le pseudo n'existe déjà dans la base à l'aide d'une fonction du modèle
            if (existeUtilisateur($_POST['pseudo'], $_POST['mail'])){
                 $_SESSION['error'] = "Le pseudo ou le mail existe déjà"; 
                $_POST = [];
                header('Location:'.baseURL.'index.php?route=inscription');
            }
        
            // On a passé toutes les conditions: on enregistre l'utilisateur dans la base, 
            // on effectue la connexion et on redirige vers l'accueil 
            inscriptionUtilisateur($_POST['pseudo'], $_POST['mail'], $_POST['pw1']); 
            $_SESSION['pseudo'] = $_POST['pseudo']; 
            header('Location:'.baseURL.'index.php');
    }
}

function connexion(){
    if (empty($_POST)) {
        // si on a reçu aucune donnée via un formulaire, on affiche le formulaire de connexion
        require('application/vues/pageConnexion.php');
    } else {
        // on vérifie que tous les champs sont remplis
        if (empty($_POST['pseudo']) || empty($_POST['pw'])) {
            // on vide le tableau $_POST pour éviter une boucle de redirection infinie
            $_POST = [];
            $_SESSION['error'] = "Merci de renseigner tous les champs";
            header('Location: ' . baseURL . 'index.php?route=connexion');
        }

        // Vérification de l'authentification de l'utilisateur à l'aide d'une fonction du modèle
        if (connexionUtilisateur($_POST['pseudo'], $_POST['pw'])) {
            $_SESSION['pseudo'] = $_POST['pseudo'];
            header('Location: ' . baseURL . 'index.php');
        } else {
            $_SESSION['error'] = "Identifiants ou mot de passe incorrects";
            $_POST = [];
            header('Location: ' . baseURL . 'index.php?route=connexion');
        }
    }
}

function deconnexion(){
    unset($_SESSION['pseudo']);
    header('Location: ' . baseURL . 'index.php');
}
?>