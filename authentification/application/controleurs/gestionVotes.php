<?php
session_start();
// Inclusion du modèle des votes
require('../modeles/votes.php');

if (empty($_POST)) {
    require('application/vues/ajoutPhoto.php');
} else {
    // Récupérer les données du formulaire
    $evaluateur = $_SESSION['pseudo'];
    $id_photo = $_POST['id_photo'];
    $note = $_POST['note'];
    // Appeler la fonction d'importation de la photo du modèle
    ajoutVoteBDD($id_photo, $evaluateur, $note);

    // Effectuer la redirection vers la page d'accueil
    header("Location: ../../index.php");
    exit();
}
?>