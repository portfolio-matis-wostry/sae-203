<?php
require('application/modeles/photos.php');

function ajoutPhoto()
{
    if (empty($_POST)) {
        require('application/vues/ajoutPhoto.php');
    } else {
        // Récupérer les données du formulaire
        $auteur = $_SESSION['pseudo'];
        $description = $_POST['description'];
        $date = date('Y-m-d H:i:s');
        $titre = $_POST['titre'];
        $nomTemporaire = $_FILES['photo']['tmp_name'];

        // Appeler la fonction d'importation de la photo du modèle
        importPhoto($auteur, $description, $date, $titre, $nomTemporaire);
            
        // Effectuer la redirection vers la page d'accueil
        header("Location: index.php");
        exit();
    }
}



function affichePhoto($id,$auteur, $description, $chemin, $date, $titre) {
    // Affichez les informations de la photo selon votre mise en forme souhaitée
    $infos="<div>
    <img src='$chemin' alt='$titre'>
    <h3>$titre</h3>
    <p>Auteur : $auteur</p>
    <p>Date : $date</p>
    <p>Description : $description</p>
    </div>
    <div>";
    
    $formul='';

    if (isset($_SESSION['pseudo'])) {$formul=
    "<form action='application/controleurs/gestionVotes.php' method='POST'>
    <input type='hidden' name='id_photo' value='$id'>
    <div class='rating'>
    <label for='note'>Vote : </label>
    <input type='range' name='note' id='note' value='0' min='1' max='5' step='1' />
    <div class='stars'>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    </div>
    <p class='selected-text'>0 étoile(s) sélectionnée(s)</p>
    </div>
    <input type='submit' value='Voter'>
    </form>";
    }

    $infoSup=
    "</div>
     la moyenne de la photo est : ".calculMoyenneVote($id);

     return $infos.$formul.$infoSup;
    } 


function affichePhotos($liste) {
    // Parcourez le tableau associatif et affichez chaque photo en utilisant la fonction affichePhoto()
    $vide='';
    foreach ($liste as $photo) {
        $vide = $vide.affichePhoto($photo['id_photo'],$photo['auteur_photo'], $photo['description_photo'], $photo['chemin_photo'], $photo['date_photo'], $photo['titre_photo']);
    }
    return $vide;
}
?>