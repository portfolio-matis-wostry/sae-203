<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta auteur ="M. Wostry">
    <link rel="stylesheet" href="<?php 'Location:'.baseURL.'/public/css/style.css'?>">
    <link rel="stylesheet" href="http://localhost/SAE203/authentification/public/css/style.css">
    <script src="http://localhost/SAE203/authentification/public/js/script.js" defer></script>
    <title><?=$title?></title>
</head>
<body>
    <!-- Barre NAV -->
    <nav>
        <?php
        if (isset($_SESSION['pseudo']))
        {
        ?>
        Connecté en tant que <?=$_SESSION['pseudo']?>
        <a href="index.php?route=deconnexion">Se déconnecter</a>
        <a href="index.php?route=ajoutPhoto">ajoutPhoto</a>
        <?php
        } else {
        ?>
        <a href="index.php?route=connexion">Se connecter</a>
        <a href="index.php?route=inscription">S'inscrire</a>
        <?php
        }
        ?>
    </nav>

    <!-- Contenu principal -->
    <main>
        <?php
            echo $content;
        ?>
    </main>
</body>
</html>