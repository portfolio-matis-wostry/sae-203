<?php ob_start(); ?>
<form action="index.php?route=ajoutPhoto" method="POST" enctype="multipart/form-data">
    <label for="titre">Titre de la photo</label>
    <input type="text" name="titre" id="titre">
    <label for="description">Description</label>
    <textarea name="description" id="description"></textarea>
    <label for="photo">Choisissez une photo</label>
    <input type="file" name="photo" id="photo">
    <button type="submit">Ajouter la photo</button>
</form>
<?php
$content = ob_get_clean();
$title = "Ajouter une photo";
require("application/vues/template.php");
?>

