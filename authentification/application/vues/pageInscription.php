<?php 
ob_start();
?>
<form action="index.php?route=inscription" method="POST">
    <?php
    // si on revient sur cette page suite à une erreur de saisie, on affiche le message d'erreur 
    if(isset($_SESSION['error'])) {
    echo '<p class-error>'.$_SESSION['error'].'</p>';
    //class error pour permettre css en rouge pour les erreurs 
    unset($_SESSION['error']);
    }
    ?>
    <label for="pseudo">Entrez votre pseudo</label>
    <input type="text" name="pseudo" id="pseudo">
    <label for="mail">Entrez votre mail</label> <input type="email" name="mail" id="mail">
    <label for="pw1">Choisissez votre mot de passe</label>
    <input type="password" name="pw1" id="pw1">
    <label for="pw2">Répétez votre mot de passe</label>
    <input type="password" name="pw2" id="pw2">
    <button type="submit">S'inscrire</button>
</form>
<?php
    $content = ob_get_clean();
    $title = "inscription";
    require('application/vues/template.php');
?>