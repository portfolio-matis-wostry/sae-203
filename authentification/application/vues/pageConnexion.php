<?php 
ob_start();
?>
<form action="index.php?route=connexion" method="POST">
    <?php
    // si on revient sur cette page suite à une erreur de saisie, on affiche le message d'erreur 
    if(isset($_SESSION['error'])) {
    echo '<p class-error>'.$_SESSION['error'].'</p>';
    //class error pour permettre css en rouge pour les erreurs 
    unset($_SESSION['error']);
    }
    ?>
    <label for="pseudo">Entrez votre pseudo</label> <input type="test" name="pseudo" id="pseudo">
    <label for="pw">Entrez votre mot de passe</label>
    <input type="password" name="pw" id="pw">
    <button type="submit">Se connecter</button>
</form>
<?php
    $content = ob_get_clean();
    $title = "connexion";
    require('application/vues/template.php');
?>