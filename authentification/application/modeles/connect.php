<?php
// fonction de base permettant de se connecter à la base de données
function connect() {
    $userName = 'root';
    $pw = '';
    $dbName = 'sae203etape2';
    try {
        $db = new PDO("mysql:host=localhost;dbname=$dbName",$userName,$pw);
        return $db;
    } catch(PDOException $e) {
        echo "erreur ".$e->getMessage();
        die();
    }
}
?>
