<?php
require('application/modeles/connect.php');
// fonction qui ajoute un utilisateur dans la base
function inscriptionUtilisateur($pseudo, $mail, $pw) {    
    $dbh = connect();
    $sql = "INSERT INTO utilisateur (pseudo_utilisateur, mail_utilisateur ,mdp_utilisateur) VALUES(? ,?, ?)";
    $sth = $dbh->prepare($sql);
    $sth->execute(array($pseudo, $mail, $pw));
    $dbh = null;    
}

function existeUtilisateur($pseudo, $mail) {
    $dbh = connect();
    $sql = "SELECT * FROM utilisateur WHERE  pseudo_utilisateur = ? OR mail_utilisateur = ?";
    $sth = $dbh->prepare($sql);
    $sth->execute(array($pseudo, $mail));
    $result = $sth->fetch(PDO::FETCH_ASSOC);    
    $dbh = null;    
    if ($result == "") { return false;}
    return(true);
}

function connexionUtilisateur($pseudo, $pw) {
    $dbh = connect();
    $sql = "SELECT * FROM utilisateur WHERE  pseudo_utilisateur = ? AND mdp_utilisateur = ?";
    $sth = $dbh->prepare($sql);
    $sth->execute(array($pseudo, $pw));
    $result = $sth->fetch(PDO::FETCH_ASSOC);    
    $dbh = null;    
    if ($result == "") { return false;}
    return(true);
}
?>