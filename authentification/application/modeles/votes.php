<?php
require("../modeles/connect.php");

// Renvoie null si le vote n'existe pas
function obtenirVote($id_photo, $evaluateur) {
    $dbh = connect();
    $sql =  "SELECT valeur_vote FROM vote  WHERE photo_vote=? AND utilisateur_vote=?";
    $sth = $dbh->prepare($sql);

    $sth->execute(array($id_photo, $evaluateur));
    $result = $sth->fetch(PDO::FETCH_ASSOC);
    if (empty($result)) {
        return null;
    }
    return $result['valeur_vote'];

}

function ajoutVoteBDD($id_photo, $evaluateur, $note) {
    $dbh = connect();
    // Si une note est déjà présente pour ce couple photo/utilisateur, on commence par l'effacer
    if (obtenirVote($id_photo, $evaluateur) != null) {
        $sqlDelete = "DELETE FROM vote WHERE photo_vote=? AND utilisateur_vote=?";
        $sthDelete = $dbh->prepare($sqlDelete);
        $sthDelete->execute(array($id_photo, $evaluateur));
    }
    // Ajout du nouveau vote
    $sqlInsert = "INSERT INTO vote (photo_vote, utilisateur_vote, valeur_vote) VALUES (?, ?, ?)";
    $sthInsert = $dbh->prepare($sqlInsert);
    $sthInsert->execute(array($id_photo, $evaluateur, $note));
}

// function calculMoyenneVote($id_photo) {
//     $dbh = connect();

//     // Requête SQL pour obtenir la moyenne des notes de la photo sélectionnée
//     $sql = "SELECT AVG(valeur_vote) AS moyenne FROM vote WHERE photo_vote = ?";
//     $sth = $dbh->prepare($sql);
//     $sth->execute(array($id_photo));
//     // Récupération de la moyenne des notes
//     $row = $sth->fetch(PDO::FETCH_ASSOC);
//     $moyenne = $row['moyenne'];
//     // Arrondir la moyenne à deux décimales
//     $moyenneArrondie = round($moyenne, 2);
//     // Retourner la moyenne
//     return $moyenneArrondie;
// }

?>