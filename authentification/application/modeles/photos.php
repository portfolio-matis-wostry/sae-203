<?php
require("application/modeles/connect.php");
function importPhoto($auteur, $description, $date, $titre, $nomTemporaire)
{
    $dbh = connect();
    // On supprime les espaces dans le titre
    $titre_compact = str_replace(' ', '_', $titre);
    // On supprime les espaces dans le nom de l'auteur
    $auteur_compact = str_replace(' ', '_', $auteur);
    // On stocke la photo en l'enregistrant sous le nom auteur+titre
    // Seuls les fichiers jpeg sont acceptés
    $chemin = 'public/medias/images/' . $auteur_compact . $titre_compact . '.jpg';
    // Stockage de la photo
    move_uploaded_file($nomTemporaire, $chemin);

    // Insertion des données de l'image dans la base de données
    $stmt = $dbh->prepare("INSERT INTO photo (auteur_photo, description_photo,chemin_photo, date_photo, titre_photo ) VALUES (?, ?, ?, ?, ?)");
    $stmt->execute([$auteur, $description,$chemin, $date, $titre ]);
}

function getTroisDernieresPhotos() {
    $dbh = connect(); // Connexion à la base de données
    
    // Requête SQL pour récupérer les trois photos les plus récentes
    $query = "SELECT * FROM photo ORDER BY id_photo DESC LIMIT 3";
    $stmt = $dbh->prepare($query);
    $stmt->execute();
    
    // Récupération des résultats de la requête dans un tableau associatif
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    return $result;
}

function calculMoyenneVote($id_photo) {
    $dbh = connect();

    // Requête SQL pour obtenir la moyenne des notes de la photo sélectionnée
    $sql = "SELECT AVG(valeur_vote) AS moyenne FROM vote WHERE photo_vote = ?";
    $sth = $dbh->prepare($sql);
    $sth->execute(array($id_photo));
    // Récupération de la moyenne des notes
    $row = $sth->fetch(PDO::FETCH_ASSOC);
    $moyenne = $row['moyenne'];
    // Arrondir la moyenne à deux décimales
    $moyenneArrondie = round($moyenne, 2);
    // Retourner la moyenne
    return $moyenneArrondie;
}
?>