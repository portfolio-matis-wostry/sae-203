/* eslint-disable require-jsdoc */
const ratingInputs = document.querySelectorAll('.rating input[type=\'range\']');
const starsList = document.querySelectorAll('.rating .stars');
const selectedTextList = document.querySelectorAll('.rating .selected-text');

// Parcours de chaque élément input de type "range"
ratingInputs.forEach(function(ratingInput, index) {
  const stars = starsList[index].querySelectorAll('span');
  const selectedText = selectedTextList[index];

  // Écouteur d'événement pour la modification de la valeur de l'input
  ratingInput.addEventListener('input', function() {
    const value = this.value;
    updateStars(stars, value, selectedText);
  });

  // Parcours de chaque étoile
  stars.forEach(function(star, starIndex) {
    // Écouteur d'événement pour le clic sur une étoile
    star.addEventListener('click', function() {
      const value = stars.length - starIndex;
      ratingInput.value = value;
      updateStars(stars, value, selectedText);
    });
  });
});

// Fonction pour mettre à jour les étoiles
function updateStars(stars, value, selectedText) {
  for (let i = 0; i < stars.length; i++) {
    if (i < value) {
      stars[stars.length - 1 - i].classList.add('selected');
    } else {
      stars[stars.length - 1 - i].classList.remove('selected');
    }
  }
  selectedText.textContent = value + ' étoile(s) sélectionnée(s)';
}
