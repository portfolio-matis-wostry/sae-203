-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 14 juin 2023 à 14:25
-- Version du serveur : 10.4.27-MariaDB
-- Version de PHP : 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `sae203etape2`
--

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE `photo` (
  `id_photo` int(11) NOT NULL,
  `auteur_photo` varchar(128) NOT NULL,
  `description_photo` tinytext NOT NULL,
  `chemin_photo` varchar(128) NOT NULL,
  `date_photo` datetime NOT NULL,
  `titre_photo` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `photo`
--

INSERT INTO `photo` (`id_photo`, `auteur_photo`, `description_photo`, `chemin_photo`, `date_photo`, `titre_photo`) VALUES
(1, 'test', 'C\'est mon chien', 'public/medias/images/testtest.jpg', '2023-05-26 00:00:00', 'test'),
(2, 'test', 'C\'est toujours mon chien', 'public/medias/images/testTest2.jpg', '2023-05-26 00:00:00', 'Test2'),
(3, 'test', 'C\'est encore mon chien', 'public/medias/images/testTest3.jpg', '2023-05-26 00:00:00', 'Test3'),
(4, 'test', 'C\'est encore et toujours mon chien', 'public/medias/images/testTest4.jpg', '2023-05-26 00:00:00', 'Test4'),
(5, 'test', 'Un beau chien', 'public/medias/images/testTest5.jpg', '2023-05-26 00:00:00', 'Test5'),
(6, 'test', 'Chien', 'public/medias/images/testTest6.jpg', '2023-05-26 14:19:53', 'Test6'),
(8, 'test', 'C\'est rolly', 'public/medias/images/testRolly.jpg', '2023-06-01 14:16:09', 'Rolly');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `pseudo_utilisateur` varchar(128) NOT NULL,
  `mail_utilisateur` varchar(128) NOT NULL,
  `mdp_utilisateur` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`pseudo_utilisateur`, `mail_utilisateur`, `mdp_utilisateur`) VALUES
('matis', 'matis@gmail.com', 'm'),
('Max', 'max@gmail.com', 'max'),
('test', 'test@gmail.com', 't'),
('test1', 'test1@xn--gmail-jja.com', 't');

-- --------------------------------------------------------

--
-- Structure de la table `vote`
--

CREATE TABLE `vote` (
  `photo_vote` int(11) NOT NULL,
  `utilisateur_vote` varchar(128) NOT NULL,
  `valeur_vote` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `vote`
--

INSERT INTO `vote` (`photo_vote`, `utilisateur_vote`, `valeur_vote`) VALUES
(5, 'test', 1),
(6, 'test', 2),
(8, 'test', 3),
(5, 'Max', 5),
(8, 'Max', 1),
(6, 'matis', 4),
(8, 'matis', 5),
(5, 'matis', 3);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id_photo`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`pseudo_utilisateur`,`mail_utilisateur`);

--
-- Index pour la table `vote`
--
ALTER TABLE `vote`
  ADD KEY `photo` (`photo_vote`),
  ADD KEY `utilisateur` (`utilisateur_vote`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `photo`
--
ALTER TABLE `photo`
  MODIFY `id_photo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `vote`
--
ALTER TABLE `vote`
  ADD CONSTRAINT `photo` FOREIGN KEY (`photo_vote`) REFERENCES `photo` (`id_photo`),
  ADD CONSTRAINT `utilisateur` FOREIGN KEY (`utilisateur_vote`) REFERENCES `utilisateur` (`pseudo_utilisateur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
